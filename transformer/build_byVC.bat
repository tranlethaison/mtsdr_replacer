@echo off

call "C:\Program Files\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

set SRC=src/transformer.c
set SRC=%SRC% src/JIS8_to_EBCDIC_vector.c
set SRC=%SRC% src/messages.c
set SRC=%SRC% src/utilities.c
set SRC=%SRC% src/logger.c

echo Building Transformer
cl /Fe./transform.exe /I inc %SRC%

echo Cleaning objs
del -r transformer.obj
del -r JIS8_to_EBCDIC_vector.obj
del -r messages.obj
del -r utilities.obj
del -r logger.obj

echo Done!
