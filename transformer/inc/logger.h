/*Logger
History:
    180302 - SonTLT - Created
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef LOGGER_H
#define LOGGER_H


extern char *log_file; //All log messages will be appended to this file

extern bool is_log_created; //Keeps track whether the log file is created or not

extern FILE *file; //Log stream

/*Begin log process
Call this first, otherwise no log will be written
*/
void begin_log(const char *log_file_name);


/*Logs a message to log_file*/
#define log(...) \
    if(file) { \
        fprintf(file, __VA_ARGS__); \
    }


/*End log process, call this after done logging
Closes the log stream
*/
void end_log();

// Messages
extern const char ERR_OPENING_LOG_FILE_FAILED[];
extern const char WAR_LOG_FILE_EXISTS[];

#endif
