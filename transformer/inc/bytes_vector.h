/* Byte to byte converter
History:
    180220 - SonTLT - Created
*/

#ifndef BYTES_VECTOR_H
#define BYTES_VECTOR_H

const extern unsigned char JIS8_to_EBCDIC_vector[256];

#endif
