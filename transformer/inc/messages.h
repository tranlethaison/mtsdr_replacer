/* Messages
History:
    180227 - SonTLT - Created
*/

#ifndef MESSAGE_H
#define MESSAGE_H

// Errors
extern const char ERR_WRONG_PARAMS_COUNT[];

extern const char ERR_INVALID_FLAGS[];

extern const char ERR_INPUT_FILE_NOT_EXISTS[];
extern const char ERR_OPENING_INPUT_FILE_FAILED[];

extern const char ERR_OPENING_OUTPUT_FILE_FAILED[];

extern const char ERR_READING_RDW_FAILED[];
extern const char ERR_INPUT_FILE_FORMAT_RDW[];

extern const char ERR_INPUT_FILE_FORMAT_DATA[];
extern const char ERR_READING_DATA_FAILED[];

extern const char ERR_WRITING_RDW_FAILED[];
extern const char ERR_WRITING_DATA_FAILED[];
extern const char ERR_WRITING_CRLF_FAILED[];

// Warnings
extern const char WAR_OUTPUT_FILE_EXISTS[];

// Logs
extern const char LOG_TIME_FORMAT[];
extern const char LOG_START[];
extern const char LOG_INPUT[];
extern const char LOG_OUTPUT[];
extern const char LOG_RESULT[];
extern const char LOG_RECORDS_COUNT[];
extern const char LOG_END[];
extern const char LOG_EXIT_CODE[];

#endif
