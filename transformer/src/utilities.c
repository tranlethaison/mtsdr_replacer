/* C utilities
History:
    180227 - SonTLT - Created
*/

#include "utilities.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>


/**/
bool dose_file_exists(const char *filename) {
    FILE *fp = fopen(filename, "r");
    if(fp) 
        fclose(fp);

    return (fp != NULL);
}


/**/
bool is_value_in_array(void *value, void **array, size_t size) {
    for(int i=0; i < size; i++) {
        if(strcmp(array[i], value) == 0)
            return true;
    }
    return false;
}

/**/
const char * get_current_date_time(const char *format) {
    time_t raw;
    time(&raw);

    if(format) {
        char *formatted = (char *) malloc(64);
        strftime(formatted, 64, format, localtime(&raw));
        return formatted;
    }
    else
        return asctime(localtime(&raw));   
}
