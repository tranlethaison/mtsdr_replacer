/* ＭＴＳＤＲ廃止に伴う互換ツール v.1.0
History:
    180220 - SonTLT - Created
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "utilities.h"
#include "logger.h"

#include "bytes_vector.h"
#include "messages.h"

/*Record Descriptor Word (RDW)
The record descriptor word is a 4 byte field describing the record.
The first 2 bytes contain the length (LL) of the logical record (including the 4 byte RDW).
All bits of the third and fourth bytes must be 0.
*/
#define RDW_SIZE 4

/*Get record size from RDW*/
size_t get_record_size(unsigned char *rdw) {
    // bin -> dec
    return (size_t)(
        (rdw[0]) << 8 |
        (rdw[1])
    );
}


/*Check if any 4 bytes is a RDW
Conditions:
> Record size (from 1st and 2nd bytes) is greater than RDW size
> All bits of the 3rd and 4th bytes are 0s.
*/
bool is_rdw(unsigned char *rdw) {
    return (
        (get_record_size(rdw) > RDW_SIZE) &&
        (rdw[2] == 0x00) &&
        (rdw[3] == 0x00)
    );
}


/*Byte-to-byte converter, from JIS8 to EBCDIC
> Return: the pointer to new data
*/
unsigned char * from_JIS_get_EBCDIC(unsigned char *JIS_data, size_t data_size) {
    unsigned char *EBCDIC_data = (unsigned char *) malloc(data_size);

    for(int i = 0; i < data_size; i++) {
        EBCDIC_data[i] = JIS8_to_EBCDIC_vector[JIS_data[i]];
    }

    return EBCDIC_data;
}


/*Ouput writer, base on assigned flags
> Return: true if writing succeed
*/
bool write(char *flag_0, char *flag_1, unsigned char *rdw, unsigned char *data, size_t data_size, FILE *output_stream) {
    // Settings based on flag_0
    size_t rdw_count;
    size_t CRLF_count;
    if(strcmp(flag_0, "0") == 0) {
        rdw_count = 0;
        CRLF_count = 1;
    }
    else if(strcmp(flag_0, "1") == 0) {
        rdw_count = 1;
        CRLF_count = 1;
    }
    else if(strcmp(flag_0, "2") == 0) {
        rdw_count = 1;
        CRLF_count = 0;
    }
    else {
        printf("\n[Error: invalid flag 0][%s]", flag_0);
        log("\n[Error: invalid flag 0][%s]", flag_0);
        return false;
    }

    // Settings based on flag_1
    unsigned char *data_to_write;
    if(strcmp(flag_1, "0") == 0) {
        data_to_write = data;
    }
    else if(strcmp(flag_1, "1") == 0) {
        data_to_write = from_JIS_get_EBCDIC(data, data_size);
        //To-do_0: free(data_to_write) after output writing.
    }
    else {
        printf("\n[Error: invalid flag 1][%s]", flag_1);
        log("\n[Error: invalid flag 1][%s]", flag_1);
        return false;
    }

    // Output writing
    fwrite(rdw, RDW_SIZE, rdw_count, output_stream);
    if(ferror(output_stream)) {
        printf("\n[Error: %s][%s]", ERR_WRITING_RDW_FAILED, strerror(errno));
        log("\n[Error: %s][%s]", ERR_WRITING_RDW_FAILED, strerror(errno));
        return false;
    }
    fwrite(data_to_write, data_size, 1, output_stream);
    if(ferror(output_stream)) {
        printf("\n[Error: %s][%s]", ERR_WRITING_DATA_FAILED, strerror(errno));
        log("\n[Error: %s][%s]", ERR_WRITING_DATA_FAILED, strerror(errno));
        return false;
    }
    fwrite("\r\n", 2, CRLF_count, output_stream);
    if(ferror(output_stream)) {
        printf("\n[Error: %s][%s]", ERR_WRITING_CRLF_FAILED, strerror(errno));
        log("\n[Error: %s][%s]", ERR_WRITING_CRLF_FAILED, strerror(errno));
        return false;
    }

    //To-do_0:
    if(strcmp(flag_1, "1") == 0) {
        free(data_to_write);
    }

    return true;
}


/*User manual*/
void print_usage() {
    printf("\n引数: flag_0 flag_1 inputFile outputFile [/log logFile]\n");
    printf("flag_0:\n");
    printf("\t0: 出力ファイルにＲＤＷを出力しない、レコード末尾にCR/.LFを付加する\n");
    printf("\t1: 出力ファイルにＲＤＷを出力する、レコード末尾にCRLFを付加する\n");
    printf("\t2: 出力ファイルにＲＤＷを出力する、レコード末尾にCRLFを付加しない　（つまりもとのまま）\n");
    printf("flag_1:\n");
    printf("\t0: レコードデータのＡＮＫ変換は行わない　（入力レコードデータはそのまま）\n");
    printf("\t1: レコードデータのＡＮＫ変換を行う（JIS8→EBCDIC）\n");

    printf("\n引数: /help\n");
    printf("ヘルプを参考");
}


/*Flags checker*/
const char *FLAG_0_OPTIONS[3] = {"0", "1", "2"};
const char *FLAG_1_OPTIONS[2] = {"0", "1"};
bool are_flags_valid(char *flag_0, char *flag_1) {
    return (
        (is_value_in_array(flag_0, FLAG_0_OPTIONS, 3)) &&
        (is_value_in_array(flag_1, FLAG_1_OPTIONS, 2))
    );
}


/**/
int main(int argc, char **argv) {
    errno = 0;

    printf("ＭＴＳＤＲ廃止に伴う互換ツール v.1.0");

    // Check arguments
    char *flag_0, *flag_1, *input, *output;
    if((argc == 7) && (strcmp(argv[5], "/log") == 0)) {
        flag_0 = argv[1];
        flag_1 = argv[2];
        input = argv[3];
        output = argv[4];
        
        begin_log(argv[6]);
        log("%s %s\n", LOG_START, get_current_date_time(LOG_TIME_FORMAT));
    }
    else if(argc == 5) {
        flag_0 = argv[1];
        flag_1 = argv[2];
        input = argv[3];
        output = argv[4];
    }
    else if((argc == 2) && (strcmp(argv[1], "/help") == 0)) {
        print_usage();
        return 0;
    }
    else {
        printf("\n[Error: %s]", ERR_WRONG_PARAMS_COUNT);
        return 1;
    }

    //Check flags
    if(!are_flags_valid(flag_0, flag_1)){
        printf("\n[Error: %s][flag_0: %s][flag_1: %s]", ERR_INVALID_FLAGS, flag_0, flag_1);
        log("\n[Error: %s][flag_0: %s][flag_1: %s]", ERR_INVALID_FLAGS, flag_0, flag_1);
        log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
        end_log();
        return 1;
    }

    // Pointers
    FILE *input_stream = NULL;
    FILE *output_stream = NULL;
    unsigned char *data = NULL;

    //Open input stream
    if(!dose_file_exists(input)) {
        printf("\n[Error: %s][%s]", ERR_INPUT_FILE_NOT_EXISTS, input);
        log("\n[Error: %s][%s]", ERR_INPUT_FILE_NOT_EXISTS, input);
        log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
        end_log();
        return 1;
    }
    input_stream = fopen(input , "rb");
    if(!input_stream) {
        printf("\n[Error: %s][%s][%s]", ERR_OPENING_INPUT_FILE_FAILED, strerror(errno), input);
        log("\n[Error: %s][%s][%s]", ERR_OPENING_INPUT_FILE_FAILED, strerror(errno), input);
        log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
        end_log();
        return 1;
    }

    //Open output_stream
    if(dose_file_exists(output)) {
        printf("\n[Warning: %s][%s]", WAR_OUTPUT_FILE_EXISTS, output);
        log("\n[Warning: %s][%s]", WAR_OUTPUT_FILE_EXISTS, output);
    }
    output_stream = fopen(output , "wb");
    if(!output_stream) {
        printf("\n[Error: %s][%s][%s]", ERR_OPENING_OUTPUT_FILE_FAILED, strerror(errno), output);
        log("\n[Error: %s][%s][%s]", ERR_OPENING_OUTPUT_FILE_FAILED, strerror(errno), output);
        log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
        end_log();

        if(input_stream) fclose(input_stream);
        return 1;
    }

    printf("\n[Flag 0: %s][Flag 1: %s]", flag_0, flag_1);
    printf("\n[Input: %s]", input);
    printf("\n[Output: %s]\n", output);

    log("\n%s\n%s\n", LOG_INPUT, input);
    log("\n%s\n%s\n", LOG_OUTPUT, output);

    //Loop through records
    size_t record_count = 0;
    size_t byte_count = 0;
    size_t record_size = 0;
    size_t data_size = 0;
    unsigned char rdw[RDW_SIZE];
    while(1) {
        //Read record's RDW
        memset(rdw, 0, RDW_SIZE);
        fread(rdw, RDW_SIZE, 1, input_stream);
        if(feof(input_stream))
            break;
        else if(ferror(input_stream)) {
            printf("\n[Error: %s][%s]", ERR_READING_RDW_FAILED, strerror(errno));
            log("\n[Error: %s][%s]", ERR_READING_RDW_FAILED, strerror(errno));
            log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
            end_log();

            free(data);
            if(input_stream) fclose(input_stream);
            if(output_stream) fclose(output_stream);
            return 1;
        }
        else if(!is_rdw(rdw)) {
            printf("\n[Error: %s]", ERR_INPUT_FILE_FORMAT_RDW);
            log("\n[Error: %s]", ERR_INPUT_FILE_FORMAT_RDW);
            log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
            end_log();

            free(data);
            if(input_stream) fclose(input_stream);
            if(output_stream) fclose(output_stream);
            return 1;
        }

        //Read record's data
        record_size = get_record_size(rdw);
        data_size = record_size - RDW_SIZE;
        data = (unsigned char*) realloc(data, data_size);
        fread(data, data_size, 1, input_stream);
        if(feof(input_stream)) {
            printf("\n[Error: %s]", ERR_INPUT_FILE_FORMAT_DATA);
            log("\n[Error: %s]", ERR_INPUT_FILE_FORMAT_DATA);
            log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
            end_log();
            
            free(data);
            if(input_stream) fclose(input_stream);
            if(output_stream) fclose(output_stream);
            return 1;
        }
        else if(ferror(input_stream)) {
            printf("\n[Error: %s][%s]", ERR_READING_DATA_FAILED, strerror(errno));
            log("\n[Error: %s][%s]", ERR_READING_DATA_FAILED, strerror(errno));
            log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
            end_log();
            
            free(data);
            if(input_stream) fclose(input_stream);
            if(output_stream) fclose(output_stream);
            return 1;
        }

        byte_count += record_size;
        record_count++;
        printf("[Total][Records: %u][Bytes: %u]\r", record_count, byte_count);
        fflush(stdout);

        //Write output base on assigned flags
        if(!write(flag_0, flag_1, rdw, data, data_size, output_stream)) {
            log("\n%s%03u ***\n", LOG_EXIT_CODE, 1);
            end_log();

            free(data);
            if(input_stream) fclose(input_stream);
            if(output_stream) fclose(output_stream);
            return 1;
        }
    }
    printf("\nDone!");

    log("\n%s", LOG_RESULT);
    log("\n%s%u\n", LOG_RECORDS_COUNT, record_count);
    log("\n%s %s", LOG_END, get_current_date_time(LOG_TIME_FORMAT));
    log("\n%s%03u ***\n", LOG_EXIT_CODE, 0);
    end_log();

    free(data);
    if(input_stream) fclose(input_stream);
    if(output_stream) fclose(output_stream);
    return 0;
}
