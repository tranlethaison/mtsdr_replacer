/* Messages
History:
    180227 - SonTLT - Created
*/
#include "messages.h"

// Errors
const char ERR_WRONG_PARAMS_COUNT[] = "引数の指定数が違います";

const char ERR_INVALID_FLAGS[] = "invalid flags";

const char ERR_INPUT_FILE_NOT_EXISTS[] = "入力ファイルが存在しません";
const char ERR_OPENING_INPUT_FILE_FAILED[] = "opening input file failed";

const char ERR_OPENING_OUTPUT_FILE_FAILED[] = "opening output file failed";

const char ERR_READING_RDW_FAILED[] = "reading RDW failed";
const char ERR_INPUT_FILE_FORMAT_RDW[] = "入力ファイルのファイル形式が異なります";

const char ERR_INPUT_FILE_FORMAT_DATA[] = "入力ファイルのファイルのレコードサイズが異常です";
const char ERR_READING_DATA_FAILED[] = "reading record's data failed";

const char ERR_WRITING_RDW_FAILED[] = "writing RDW failed";
const char ERR_WRITING_DATA_FAILED[] = "writing data failed";
const char ERR_WRITING_CRLF_FAILED[] = " writing CRLF failed";

// Warnings
const char WAR_OUTPUT_FILE_EXISTS[] = "出力ファイルがすでに存在します";

// Logs
const char LOG_TIME_FORMAT[] = "%Y/%m/%d %H:%M:%S";
const char LOG_START[] = "*** DISK2MT START ***";
const char LOG_INPUT[] = "＊入力ファイル情報";
const char LOG_OUTPUT[] = "＊出力ファイル情報";
const char LOG_RESULT[] = "＊結果情報";
const char LOG_RECORDS_COUNT[] = "処理レコード数：";
const char LOG_END[] = "ディスク→ディスク変換を完了しました";
const char LOG_EXIT_CODE[] = "*** DISK2MT EXIT CODE:";
