# Test script for abnormal cases
# Created by SonTLT - 180228

$test_dir = ".\abnormal_cases"

. ".\test_runner.ps1"

Write-Host "Testing abnormal cases"

# Move to test folder
$current_dir = Get-Location
Set-Location $test_dir

# Begin test
$case = "1-1-1"
$params = @("0", "0", "0" , "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-1-2"
$params = @("0" , "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-1-3"
$params = @("0" , "0", "file1", "file2", "/log")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-1-1"
$params = @("3" , "0", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-1-2"
$params = @("A" , "0", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-1-3"
$params = @("000" , "0", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-2-1"
$params = @("0" , "2", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-2-2"
$params = @("0" , "A", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-2-2-3"
$params = @("0" , "000", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "2-1-1"
$params = @("0" , "0", "file1", "file2")
test_transform_free_params -case $case -params $params  -clean 1

$case = "2-1-2"
$params = @("0" , "0", "$input_dir\$case-in", "$output_dir\$case\$case-out", "/log", "$output_dir\$case\$case.log")
test_transform_free_params -case $case -params $params  -clean 0

$case = "2-1-3"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-4"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-5-1"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-5-2"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-5-3"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-5-4"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-5-5"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "2-1-6"
Remove-Item "$output_dir\$case\$case-out" -Force
$params = @("0" , "0", "$input_dir\$case-in", "$output_dir\$case\$case-out", "/log", "$output_dir\$case\$case.log")
test_transform_free_params -case $case -params $params  -clean 0

$case = "2-1-7"
Remove-Item "$output_dir\$case\$case-out" -Force
$params = @("0" , "0", "$input_dir\$case-in", "$output_dir\$case\$case-out", "/log", "dose_not_exists\$case.log")
test_transform_free_params -case $case -params $params  -clean 0
# End test

# Move back
Set-Location $current_dir
