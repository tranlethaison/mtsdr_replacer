# Test script for normal cases
# Created by SonTLT - 180228

. ".\test_runner.ps1"

$test_dir = ".\normal_cases"

Write-Host "Testing normal cases >>>>"

# Move to test folder
$current_dir = Get-Location
Set-Location $test_dir

# Begin test
$case = "1-1-1"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "1-1-2"
$flags = @("1", "0")
test_transform_fix_params -case $case -flags $flags

$case = "1-1-3"
$flags = @("2", "0")
test_transform_fix_params -case $case -flags $flags

$case = "1-2-1"
$flags = @("0", "1")
test_transform_fix_params -case $case -flags $flags

$case = "1-2-2"
$flags = @("1", "1")
test_transform_fix_params -case $case -flags $flags

$case = "1-2-3"
$flags = @("2", "1")
test_transform_fix_params -case $case -flags $flags

$case = "1-3-1"
$params = @("0" , "0", "$input_dir\$case-in", "$output_dir\$case\$case-out")
test_transform_free_params -case $case -params $params  -clean 1

$case = "1-4-1"
$params = @("/help")
test_transform_free_params -case $case -params $params -clean 1

$case = "2-1-5"
$flags = @("2", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-1"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-2"
$flags = @("1", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-3"
$flags = @("2", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-4"
$flags = @("0", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-5"
$flags = @("1", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-1-6"
$flags = @("2", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-1"
$flags = @("0", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-2"
$flags = @("1", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-3"
$flags = @("2", "0")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-4"
$flags = @("0", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-5"
$flags = @("1", "1")
test_transform_fix_params -case $case -flags $flags

$case = "3-1-2-6"
$flags = @("2", "1")
test_transform_fix_params -case $case -flags $flags

# Stress test
# $case = "4-1-1"
# Remove-Item "$output_dir\$case\$case-out" -Force
# $params = @("0" , "0", "$input_dir\$case-in", "$output_dir\$case\$case-out", "/log", "$output_dir\$case\$case.log")
# & $pg_dir\$pg $params
# End test

# Move back
Set-Location $current_dir
