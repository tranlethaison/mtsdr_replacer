# Test Runner for MTSDR互換ツール_チェックリスト_20180227.xls
# Created by SonTLT - 180228

$pg_dir = ""
$pg = "transform.exe"

$input_dir = "0.input"
$output_dir = "1.output"
$expect_dir = "2.expected"

function test_transform_fix_params ($case, $flags) {
    Write-Host "> CASE: $case >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    # Repair folders
    $this_output_dir = "$output_dir\$case"
    $this_expect_dir = "$expect_dir\$case"

    Remove-Item $this_output_dir -Force -Recurse
    mkdir $this_output_dir
    # -------------------------------------------------------------------

    # Define files
    $input = "$input_dir\$case-in"

    $output_name = "$case-out"
    $output = "$this_output_dir\$output_name"
    $expect_output = "$this_expect_dir\$output_name"

    $log_name = "$case.log"
    $log = "$this_output_dir\$log_name"
    $expect_log = "$this_expect_dir\$log_name"

    $screen_log_name = "$case-screen.log"
    $screen_log = "$this_output_dir\$screen_log_name"
    $expect_screen_log = "$this_expect_dir\$screen_log_name"
    # -------------------------------------------------------------------

    Write-Host ">> Execute"
    Write-Host "$pg $flags $input $output /log $log > $screen_log"
    & $pg_dir\$pg $flags $input $output /log $log > $screen_log
    # -------------------------------------------------------------------

    Write-Host "`n>> Compare output with expected:"

    Write-Host ">>> Output file"
    fc.exe /b $output $expect_output
    $output_result = $LASTEXITCODE

    Write-Host ">>> Log file"
    fc.exe $log $expect_log
    $log_result = $LASTEXITCODE

    Write-Host ">>> Screen log file"
    fc.exe　$screen_log $expect_screen_log
    $screen_log_result = $LASTEXITCODE
    # -------------------------------------------------------------------

    if(
        ($output_result -eq 0) -and
        ($log_result -eq 0) -and
        ($screen_log_result -eq 0)
    ) {
        $result = "OK"
    }
    else { $result = "NG" }
    Write-Host ">> Result: $result"
    Write-Host "< CASE: $case <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<`n`n"
}


function test_transform_free_params ($case, $params, $clean=0) {
    Write-Host "> CASE: $case >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    # Repair folders
    $this_output_dir = "$output_dir\$case"
    $this_expect_dir = "$expect_dir\$case"

    if ($clean -eq 1) {
        Remove-Item $this_output_dir -Force -Recurse
        mkdir $this_output_dir
    }
    # -------------------------------------------------------------------

    # Define files
    $screen_log_name = "$case-screen.log"
    $screen_log = "$this_output_dir\$screen_log_name"
    $expect_screen_log = "$this_expect_dir\$screen_log_name"
    # -------------------------------------------------------------------

    Write-Host ">> Execute"
    Write-Host "$pg $params > $screen_log"
    & $pg_dir\$pg $params > $screen_log
    # -------------------------------------------------------------------

    Write-Host "`n>> Compare output with expected:"
    Write-Host ">>> Screen log file"
    fc.exe　$screen_log $expect_screen_log
    $screen_log_result = $LASTEXITCODE
    # -------------------------------------------------------------------

    if($screen_log_result -eq 0) {
        $result = "OK"
    }
    else { $result = "NG" }
    Write-Host ">> Result: $result"
    Write-Host "< CASE: $case <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<`n`n"
}
